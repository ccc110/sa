// 積分 integral calculus
export function integral(f, a, b, dx = 0.01) {
  var area = 0.0
  for (var x = a; x < b; x = x + dx) {
    area = area + f(x) * dx
  }
  return area
}

