// 積分 integral calculus
export function integral(f, a, b, dx = 0.01) {
  var area = 0.0
  for (var x = a; x < b; x = x + dx) {
    area = area + f(x) * dx
  }
  return area
}

function fmul(f,g) {
  return function(x) {
    return f(x)*g(x)
  }
}

function fdot(f,g, min=0, max=2*Math.PI) {
  return function() {
    let fg = fmul(f,g)
    return integral(fg, min, max)
  }
}

let sincos = fdot(Math.sin, Math.cos)
console.log('fdot(sin,cos)=', sincos())

let sinsin2x = fdot(Math.sin, (x)=>Math.sin(2*x))
console.log('fdot(sin,sin2x)=', sinsin2x())

let sinsin = fdot(Math.sin, Math.sin)
console.log('fdot(sin,sin)=', sinsin())
