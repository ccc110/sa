function fadd(f,g) {
  return function(x) {
    return f(x)+g(x)
  }
}

function f1(x) {
  return x+1
}

function f2(x) {
  return x*x
}

let f = fadd(f1, f2)

console.log('f(3)=', f(3))
