function e(n) {
  return (1.0+1.0/n)**n
}

for (let i=1; i<100000; i++) {
  console.log(`e(${i})=${e(i)}`)
}
