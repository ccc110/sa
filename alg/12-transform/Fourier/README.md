# 傅立葉轉換

* [維基百科:傅立葉變換](https://zh.wikipedia.org/zh-tw/%E5%82%85%E9%87%8C%E5%8F%B6%E5%8F%98%E6%8D%A2)
* [如果看了此文你还不懂傅里叶变换，那就过来掐死我吧【完整版】](http://blog.jobbole.com/70549/)

## 複數

* https://github.com/infusion/Complex.js/

## 離散傅立葉轉換 (慢速)

* Discrete Fourier Transform in JavaScript -- https://gist.github.com/shovon/c3ca8e59a5cf277c947a


## 快速傅立葉轉換

* https://github.com/dntj/jsfft
* https://github.com/vail-systems/node-fft
* https://github.com/JensNockert/fft.js

## 二維傅立葉變換

* [https://zh.wikipedia.org/wiki/%E4%BA%8C%E7%B6%AD%E5%82%85%E7%AB%8B%E8%91%89%E8%AE%8A%E6%8F%9B](https://zh.wikipedia.org/wiki/%E4%BA%8C%E7%B6%AD%E5%82%85%E7%AB%8B%E8%91%89%E8%AE%8A%E6%8F%9B)